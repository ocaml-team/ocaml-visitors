Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: visitors
Upstream-Contact: François Pottier <Francois.Pottier@inria.fr>
Source: https://gitlab.inria.fr/fpottier/visitors.git

Files: *
Copyright: © 2016, 2017 INRIA
License: LGPL-2.1
Comment:
 the copyright holder has been confirmed in a personal mail by the author.

Files: debian/*
Copyright: © 2017 Ralf Treinen <treinen@debian.org>
License: LGPL-2.1

Files: test/cil_types.ml test/cil_types.ml.orig test/cil_types_polymorphic.ml
Copyright: (C) 2001-2003
  George C. Necula    <necula@cs.berkeley.edu>
  Scott McPeak        <smcpeak@cs.berkeley.edu>
  Wes Weimer          <weimer@cs.berkeley.edu>
  Ben Liblit          <liblit@cs.berkeley.edu>
License: BSD-3-clause
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:
  .
  1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
  .
  2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
  .
  3. The names of the contributors may not be used to endorse or
  promote products derived from this software without specific prior
  written permission.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License version 2.1 as published by the Free Software Foundation.
 .
 On Debian systems, the complete text of the latest GNU Lesser General
 Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.
